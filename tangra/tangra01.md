# Tangra version 3.5 beta sous Linux


## 1. Installation

L'installation a été faite à partir du lien suivant : http://www.hristopavlov.net/Tangra3/

Les indication données sur la page d'installation sont correctes. Mais il faut prendre garde au moment du téléchargement de la version.

URL : http://www.hristopavlov.net/Tangra3/

Ne pas prendre la version 32 bits, si l'on a installé un système 64 bits.
(cela génère des messages d'erreurs).

## 2. Problème rencontré avec un fichier AVI

Si l'on veut expérimenter l'exemple développé dans la documentation, on sera déçu :

Le fichier vidéo proposé, au format AVI ne sera pas chargé sous Linux.

Cette impossibilité est clairement indiquée dans le descriptif http://www.hristopavlov.net/Tangra3/

